# Birds Are not Real

![Drawing](Pictures/gesamtv5.png "")

## Hier findest du die Laserpläne
![Drawing](Pictures/Laserteile.jpg "")
[https://gitlab.com/hacklabor/ndw2019/birds-are-not-real/tree/master/Drawings/LasercutV2.0](https://gitlab.com/hacklabor/ndw2019/birds-are-not-real/tree/master/Drawings/LasercutV2.0)
als Material wurde 1,8mm MDF verwendet zu bekommen bei Toom Baumarkt https://toom.de/p/modellbauplatte-200-x-500-mm/7170449

----

## Hier findest du die Software
![Drawing](Pictures/Software.jpg "")
[https://gitlab.com/hacklabor/ndw2019/birds-are-not-real/tree/master/Software/bird01](https://gitlab.com/hacklabor/ndw2019/birds-are-not-real/tree/master/Software/bird01)

----
## Elektronik Bauteile

 
- Esp8266 Wemos mini [https://www.amazon.de/dp/B076FBY2V3/?th=1](https://www.amazon.de/dp/B076FBY2V3/?th=1) 

- https://wiki.wemos.cc/products:retired:d1_mini_v2.2.0

- Wemos Mini matrix LED Shield  https://www.makershop.de/module/led/8x8-matrix-led/
- https://wiki.wemos.cc/products:d1_mini_shields:matrix_led_shield 

- Batteriehalter https://www.amazon.de/ARCELI-Battery-Shield-Raspberry-Arduino/dp/B07M78S1M8


Das bekommst du natürlich auch alles bei https://de.aliexpress.com/ zu anderen Preisen.

----
## Zusammenbau

- Holzteile verleimen 
 ![Drawing](Pictures/RealLaser.jpg "") ![Drawing](Pictures/Gehaeuse_leer.jpg "")
- mti Tesafilm ein Scharnier für obere Klappe bauen
- Matrix mit ESP verlöten (so dicht wie möglich); Notfalls die Pins von der bereits aufgelöteten Matrixanzeige kürzen damit es keine Kurzschlüsse gibt ![Drawing](Pictures/Matrix_u_ESP.jpg "")
- Matrix + Esp mit einem Kabel an Batteriehalter / Ladeeinheit verbinden (+5V rot und GND grün)![Drawing](Pictures/Anschluss.jpg "")
- dann erst die Matrixeinheit montieren danach die Batterieeinheit; Bei der Montage der Matrixeinheit darauf achten, dass der Programieranschluss nach oben zeigt ;). Auf dem Bild ist es falsch! Da macht das Programmieren keinen Spass![Drawing](Pictures/Montageinnen.jpg "")   
- Die Matrixeinheit kann mann gegen Rausfallen mit gefaltetetn Papier zwischen ESP und Trennwand sichern. Oder halt einkleben.

----
## Final

- Software aus Repro aufspielen --> oder eigene Schreiben
- have Fun


----
![](Pictures/Bild0.jpg "") 
![](Pictures/Bild1.jpg "")
![](Pictures/Bild2.jpg "") 
![](Pictures/Bild3.jpg "") 
![](Pictures/Twitter.jpg "") 
https://twitter.com/hacklabor/status/1177248750345101315?s=20
 


![](Pictures/Gesamt.jpg "") 

----

